//
//  ContainerViewInterface.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 30/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import Foundation
import UIKit

class containerViewInterface {
    
    func addSubview (subView:UIView, toView parentView:UIView, gapTop: Int, gapLead: Int) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(gapLead)-[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(gapTop)-[subView]|",
                                                                 options: [], metrics: nil, views: viewBindingsDict))
    }
    
}
