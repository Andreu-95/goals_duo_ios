//
//  GoalDescriptionViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 30/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class GoalDescriptionViewController: UIViewController {

    
    @IBOutlet weak var goalTitle: UILabel!
    @IBOutlet weak var goalDescription: UILabel!
    @IBOutlet weak var goalFinished: UILabel!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var goalIcon: UIImageView!
    
    override func viewDidLoad() {
        goalTitle.text = currentGoalsTitle[currentGoalLoading]
        goalDescription.text = currentGoalsDescription[currentGoalLoading]
        goalFinished.text = ""
        goalIcon.image = currentGoalsIcon[currentGoalLoading]
        
        threeButton.tag = 10 + currentGoalLoading
        threeButton.addTarget(self, action: #selector(mostrarAlert(_:)), for: .touchUpInside)
     
        super.viewDidLoad()
    }
    
    func mostrarAlert(_ sender: UIButton) {
        let alertCurrent = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let editGoal = UIAlertAction(title: "Edit this goal", style: .default) { (action) in
            print("Editando")
            self.performSegue(withIdentifier: "editGoalSegue", sender: self)
        }
        let deleteGoal = UIAlertAction(title: "Delete this goal", style: .default) { (action) in
            print("Borrando")
        }
        let cancelGoal = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alertCurrent.addAction(editGoal)
        alertCurrent.addAction(deleteGoal)
        alertCurrent.addAction(cancelGoal)
        
        self.present(alertCurrent, animated: true, completion: nil)
        alertCurrent.view.tintColor = UIColor(red:1.00, green:0.48, blue:0.00, alpha:1.0)
        print("Mostrando")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nav = segue.destination as! UINavigationController
        let destination = nav.topViewController as! TableEditGoalViewController
        
        destination.titleC = "Edit Goal"
    }
}
