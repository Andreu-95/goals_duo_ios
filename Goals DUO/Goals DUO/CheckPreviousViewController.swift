//
//  CheckPreviousViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra on 3/7/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class CheckPreviousViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        var count = 0
        for _ in initGoal..<endGoal {
            let goalCheck = UIImageView(frame: CGRect(x: 73 * count + 3, y: 17, width: 70, height: 70))
            
            goalCheck.image = #imageLiteral(resourceName: "goalCompleted")
            
            self.view.addSubview(goalCheck)
            count += 1
        }
        count = 0
    }
}
