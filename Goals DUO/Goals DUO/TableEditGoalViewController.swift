//
//  TableEditGoalViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 3/7/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class TableEditGoalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var titleC: String?
    @IBOutlet weak var currentGoalTitle: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentGoalTitle.title = titleC!
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellName = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! NameGoalTableViewCell
        let cellIcon = tableView.dequeueReusableCell(withIdentifier: "iconCell") as! IconGoalTableViewCell
        let cellDrop = tableView.dequeueReusableCell(withIdentifier: "dropCell") as! DropGoalTableViewCell
        
        switch indexPath.row {
        case 0:
            cellName.setupNameCell()
            return cellName
        case 1:
            return cellIcon
        default:
            return cellDrop
        }
    }
    @IBAction func backCurrent(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
