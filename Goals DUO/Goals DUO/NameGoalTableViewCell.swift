//
//  NameGoalTableViewCell.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 3/7/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit
import BMInputBox

class NameGoalTableViewCell: UITableViewCell {

    @IBOutlet weak var newNameGoal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupNameCell() {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(abrirTextInput))
        newNameGoal.addGestureRecognizer(recognizer)
    }
    
    func abrirTextInput() {
        print("InputBox")
        
        let inputBox = BMInputBox.boxWithStyle(.plainTextInput)
        
        inputBox.title = "Goal Name"
        inputBox.message = "Enter the name for your goal"
        inputBox.submitButtonText = "OK"
        inputBox.cancelButtonText = "Cancel"
        
        inputBox.onSubmit = {(value: AnyObject...) in
            for text in value {
                if text is String {
                    self.newNameGoal.text = text as? String
                }
            }
        }
        
        inputBox.show()
    }

}
