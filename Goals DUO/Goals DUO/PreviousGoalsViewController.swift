//
//  PreviousGoalsViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 3/7/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class PreviousGoalsViewController: UIViewController {

    @IBOutlet weak var previousGoals: UIView!
    weak var currentViewController: UIViewController?
    
    let cvi = containerViewInterface()
    
    override func viewDidLoad() {
        
        for _ in 0..<2 {
            self.currentViewController = self.storyboard?.instantiateViewController(withIdentifier: "previousDescription")
            self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChildViewController(self.currentViewController!)
            cvi.addSubview(subView: self.currentViewController!.view, toView: self.previousGoals, gapTop: 103 * previousGoalLoading + 3, gapLead: 3)
            previousGoalLoading += 1
        }
        
        super.viewDidLoad()
    }

}
