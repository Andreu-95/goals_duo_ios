//
//  CurrentGoalsViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 30/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class CurrentGoalsViewController: UIViewController {

    @IBOutlet weak var scrollGoals: UIView!
    weak var currentViewController: UIViewController?
    weak var currentViewController2: UIViewController?
    
    let cvi = containerViewInterface()

    override func viewDidLoad() {
        var space = 0
        
        for _ in 0..<2 {
            self.currentViewController = self.storyboard?.instantiateViewController(withIdentifier: "goalDescription")
            self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChildViewController(self.currentViewController!)
            cvi.addSubview(subView: self.currentViewController!.view, toView: self.scrollGoals, gapTop: 103 * currentGoalLoading + (space) * 100 + 3, gapLead: 3)
            currentGoalLoading += 1
            
            var numberGoals = currentGoalsChecks[currentCheckLoading]
            
            while (numberGoals > 0) {
                self.currentViewController2 = self.storyboard?.instantiateViewController(withIdentifier: "goalChecks")
                self.currentViewController2!.view.translatesAutoresizingMaskIntoConstraints = false
                self.addChildViewController(self.currentViewController2!)
                cvi.addSubview(subView: self.currentViewController2!.view, toView: self.scrollGoals, gapTop: 100 * space + 103 * currentGoalLoading, gapLead: 3)
                space += 1
                numberGoals -= 5
                initGoal += 5
                if (numberGoals > 5) {
                    endGoal += 5
                } else {
                    endGoal += numberGoals
                }
            }
            initGoal = 0
            endGoal = 5
            currentCheckLoading += 1
        }
        
        super.viewDidLoad()
    }
}
