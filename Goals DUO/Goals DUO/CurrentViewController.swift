//
//  CurrentViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 30/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class CurrentViewController: UIViewController {

    @IBOutlet weak var contentViewCurrent: UIView!
    weak var currentViewController: UIViewController?
    
    let cvi = containerViewInterface()
    
    override func viewDidLoad() {
        if currentGoals == 0 {
            self.currentViewController = self.storyboard?.instantiateViewController(withIdentifier: "noGoals")
            self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChildViewController(self.currentViewController!)
            cvi.addSubview(subView: self.currentViewController!.view, toView: self.contentViewCurrent, gapTop: 0, gapLead: 0)
        } else {
            self.currentViewController = self.storyboard?.instantiateViewController(withIdentifier: "withGoals")
            self.currentViewController!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChildViewController(self.currentViewController!)
            cvi.addSubview(subView: self.currentViewController!.view, toView: self.contentViewCurrent, gapTop: 0, gapLead: 0)
        }
        
        super.viewDidLoad()
    }
    
    
    @IBAction func addNewGoal(_ sender: Any) {
        self.performSegue(withIdentifier: "addGoalSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nav = segue.destination as! UINavigationController
        let destination = nav.topViewController as! TableEditGoalViewController
        
        destination.titleC = "Add Goal"
    }
}
