//
//  GoalsTabVarViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 27/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class GoalsTabVarViewController: UITabBarController {

    
    @IBOutlet weak var goalsTabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UIApplication.shared.statusBarFrame.size.height
        goalsTabBar.frame = CGRect(x: 0, y:  goalsTabBar.frame.size.height + 15, width: goalsTabBar.frame.size.width, height: goalsTabBar.frame.size.height)
    }
}
