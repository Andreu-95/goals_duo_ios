//
//  CheckGoalsViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 30/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class CheckGoalsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var count = 0
        for j in initGoal..<endGoal {
            let goalCheck = UIImageView(frame: CGRect(x: 73 * count + 3, y: 17, width: 70, height: 70))
                    
            if (currentGoalsCheckState[currentCheckLoading][j] == "Check") {
                goalCheck.image = #imageLiteral(resourceName: "checkGoal")
                //goalCheck.isUserInteractionEnabled = false
            } else {
                goalCheck.image = #imageLiteral(resourceName: "emptyGoal")
                goalCheck.isUserInteractionEnabled = true
                let recognizer = UITapGestureRecognizer(target: self, action: #selector(changeImage(recognizer:)))
                goalCheck.addGestureRecognizer(recognizer)
            }
                    
            self.view.addSubview(goalCheck)
            count += 1
        }
        count = 0
    }
    
    func changeImage(recognizer: UITapGestureRecognizer) {
        let imageTapped = recognizer.view as! UIImageView
        imageTapped.image = #imageLiteral(resourceName: "checkGoal")
        imageTapped.isUserInteractionEnabled = false
    }

}
