//
//  PreviousDescriptionViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 3/7/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class PreviousDescriptionViewController: UIViewController {

    
    @IBOutlet weak var goalTitle: UILabel!
    @IBOutlet weak var goalDescription: UILabel!
    @IBOutlet weak var goalFinished: UILabel!
    @IBOutlet weak var goalIconP: UIImageView!
    @IBOutlet weak var threeButton: UIButton!
    
    override func viewDidLoad() {
        goalTitle.text = previousGoalsTitle[previousGoalLoading]
        goalDescription.text = previousGoalsDescription[previousGoalLoading]
        goalFinished.text = previousGoalsFinished[previousGoalLoading]
        goalIconP.image = previousGoalsIcon[previousGoalLoading]
        
        threeButton.tag = 100 + previousGoalLoading
        threeButton.addTarget(self, action: #selector(mostrarAlert(_:)), for: .touchUpInside)
        
        super.viewDidLoad()
    }
    
    func mostrarAlert(_ sender: UIButton) {
        let alertCurrent = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let viewGoal = UIAlertAction(title: "View this goal", style: .default) { (action) in
            print("Viendo")
            self.performSegue(withIdentifier: "viewGoalSegue", sender: self)
        }
        let repeatGoal = UIAlertAction(title: "Repeat this goal", style: .default) { (action) in
            print("Repitiendo")
        }
        let deleteGoal = UIAlertAction(title: "Delete this goal", style: .default) { (action) in
            print("Borrando")
        }
        let cancelGoal = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alertCurrent.addAction(viewGoal)
        alertCurrent.addAction(repeatGoal)
        alertCurrent.addAction(deleteGoal)
        alertCurrent.addAction(cancelGoal)
        
        self.present(alertCurrent, animated: true, completion: nil)
        alertCurrent.view.tintColor = UIColor(red:1.00, green:0.48, blue:0.00, alpha:1.0)
        print("Mostrando")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nav = segue.destination as! UINavigationController
        let destination = nav.topViewController as! PreviousGoalDetailViewController
        
        destination.titleP = goalTitle.text
        destination.descriptionP = goalDescription.text
        destination.finishedP = goalFinished.text
        let index = previousGoalsFinished.index(of: goalFinished.text!)
        destination.imageP = previousGoalsImage[index!]
        destination.checksP = 5
    }

}
