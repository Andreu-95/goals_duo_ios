//
//  PreviousGoalDetailViewController.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 3/7/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import UIKit

class PreviousGoalDetailViewController: UIViewController {

    var titleP: String?
    var descriptionP: String?
    var finishedP: String?
    var imageP: UIImage?
    var checksP: Int!
    
    @IBOutlet weak var detailIcon: UIImageView!
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailDescription: UILabel!
    @IBOutlet weak var detailEnd: UILabel!
    @IBOutlet weak var titleBar: UINavigationItem!
    @IBOutlet var scrollGoals: UIView!
    weak var currentViewController2: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleBar.title = titleP
        detailIcon.image = imageP
        detailTitle.text = titleP
        detailDescription.text = descriptionP
        detailEnd.text = finishedP
        
        var numberPrevious = checksP!
        var space = 0
        let cvi = containerViewInterface()
        
        while (numberPrevious > 0) {
            self.currentViewController2 = self.storyboard?.instantiateViewController(withIdentifier: "previousChecks")
            self.currentViewController2!.view.translatesAutoresizingMaskIntoConstraints = false
            self.addChildViewController(self.currentViewController2!)
            cvi.addSubview(subView: self.currentViewController2!.view, toView: self.scrollGoals, gapTop: 100 * space + 253, gapLead: 3)
            space += 1
            numberPrevious -= 5
            initGoal += 5
            if (numberPrevious > 5) {
                endGoal += 5
            } else {
                endGoal += numberPrevious
            }
        }
        initGoal = 0
        endGoal = 5
        currentCheckLoading += 1
    }
    @IBAction func backDetail(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
