//
//  GlobalVars.swift
//  Goals DUO
//
//  Created by Andrés Guerra Vásconez on 30/6/17.
//  Copyright © 2017 Andrés Guerra Vásconez. All rights reserved.
//

import Foundation
import UIKit

var currentGoals = 2
var previousGoals = 2

var currentGoalLoading = 0
var currentCheckLoading = 0
var previousGoalLoading = 0

var initGoal = 0
var endGoal = 5

var currentGoalsTitle: [String] = ["Go for a walk", "Have a meal with family"]
var currentGoalsDescription: [String] = ["Do this 5 times this week to complete", "Do this 7 times this week to complete"]
var currentGoalsIcon: [UIImage] = [#imageLiteral(resourceName: "cup"), #imageLiteral(resourceName: "star")]
var currentGoalsChecks: [Int] = [5, 7]
var currentGoalsCheckState: [[String]] = [["Check", "Empty", "Empty", "Empty", "Empty"],
                                                  ["Check", "Empty", "Empty", "Empty", "Empty", "Empty", "Empty"]]

var previousGoalsTitle: [String] = ["Go for a walk", "Visit a friend"]
var previousGoalsDescription: [String] = ["Do this 5 times this week to complete", "Do this 5 times this week to complete"]
var previousGoalsIcon: [UIImage] = [#imageLiteral(resourceName: "goalCompleted"), #imageLiteral(resourceName: "goalFailed")]
var previousGoalsImage: [UIImage] = [#imageLiteral(resourceName: "cup"), #imageLiteral(resourceName: "star")]
var previousGoalsFinished: [String] = ["Finished 18 May 2016", "Finished 18 May 2016"]
